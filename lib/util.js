'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _safe = require('colors/safe');

var _safe2 = _interopRequireDefault(_safe);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _child_process = require('child_process');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_safe2.default.enabled = true;

_safe2.default.setTheme({
    /*SILLY: 'rainbow',
    INPUT: 'grey',
    VERBOSE: 'cyan',
    PROMPT: 'grey',
    INFO: 'green',
    DATA: 'grey',
    HELP: 'cyan',
    WARN: 'yellow',
    DEBUG: 'blue',
    ERROR: 'red',*/
    '变更': 'bgYellow',
    '删除': 'bgMagenta',
    '执行': 'blue',
    '压缩': 'blue',
    '信息': 'grey',
    '完成': 'green',
    '创建': 'green',
    '监听': 'magenta',
    '错误': 'red',
    '测试': 'red',
    '拷贝': 'yellow',
    '编译': 'blue',
    '写入': 'green'
});

exports.default = {
    seqPromise: function seqPromise(promises) {
        return new Promise(function (resolve, reject) {

            var count = 0;
            var results = [];

            var iterateeFunc = function iterateeFunc(previousPromise, currentPromise) {
                return previousPromise.then(function (result) {
                    if (count++ !== 0) results = results.concat(result);
                    return currentPromise(result, results, count);
                }).catch(function (err) {
                    return reject(err);
                });
            };

            promises = promises.concat(function () {
                return Promise.resolve();
            });

            promises.reduce(iterateeFunc, Promise.resolve(false)).then(function (res) {
                resolve(results);
            });
        });
    },
    exec: function exec(cmd, quite) {
        return new Promise(function (resolve, reject) {
            var fcmd = (0, _child_process.exec)(cmd, function (err, stdout, stderr) {
                if (err) {
                    reject(err);
                } else {
                    resolve(stdout, stderr);
                }
            });
            fcmd.stdout.on('data', function (chunk) {
                !quite && process.stdout.write(chunk);
            });
            fcmd.stderr.on('data', function (chunk) {
                !quite && process.stdout.write(chunk);
            });
        });
    },


    currentDir: process.cwd(),
    cliDir: __dirname,

    isFunction: function isFunction(fn) {
        return typeof fn === 'function';
    },
    isString: function isString(obj) {
        return toString.call(obj) === '[object String]';
    },
    isObject: function isObject(obj) {
        return toString.call(obj) === '[object Object]';
    },
    isNumber: function isNumber(obj) {
        return toString.call(obj) === '[object Number]';
    },
    isBoolean: function isBoolean(obj) {
        return toString.call(obj) === '[object Boolean]';
    },
    isArray: function isArray(obj) {
        return Array.isArray(obj);
    },
    isFile: function isFile(p) {
        p = (typeof p === 'undefined' ? 'undefined' : _typeof(p)) === 'object' ? _path2.default.join(p.dir, p.base) : p;
        if (!_fs2.default.existsSync(p)) {
            return false;
        }
        return _fs2.default.statSync(p).isFile();
    },
    isDir: function isDir(p) {
        if (!_fs2.default.existsSync(p)) {
            return false;
        }
        return _fs2.default.statSync(p).isDirectory();
    },
    isOriUrl: function isOriUrl(p) {
        // if(){

        // }
    },

    /**
     * xml dom 对 TEXT_NODE 和 ATTRIBUTE_NODE 进行转义。
     */
    decode: function decode(content) {
        var pmap = ['<', '&', '"'];
        var amap = ['&lt;', '&amp;', '&quot;'];
        var reg = new RegExp('(' + amap[0] + '|' + amap[1] + '|' + amap[2] + ')', 'ig');
        return content.replace(reg, function (match, m) {
            return pmap[amap.indexOf(m)];
        });
    },
    encode: function encode(content, start, end) {
        start = start || 0;
        end = end || content.length;

        var buffer = [];
        var pmap = ['<', '&', '"'];
        var amap = ['&lt;', '&amp;', '&quot;'];

        var i = 0,
            c = void 0;
        for (var _i = 0, len = content.length; _i < len; _i++) {
            if (_i < start || _i > end) {
                buffer.push(content[_i]);
            } else {
                c = pmap.indexOf(content[_i]);
                buffer.push(c === -1 ? content[_i] : amap[c]);
            }
        }
        return buffer.join('');
    },
    unique: function unique(arr) {
        var tmp = {},
            out = [];
        arr.forEach(function (v) {
            if (!tmp[v]) {
                tmp[v] = 1;
                out.push(v);
            }
        });
        return out;
    },
    unlink: function unlink(p) {
        var rst = '';
        p = (typeof p === 'undefined' ? 'undefined' : _typeof(p)) === 'object' ? _path2.default.join(p.dir, p.base) : p;
        try {
            rst = _fs2.default.unlinkSync(p);
        } catch (e) {
            rst = null;
        }
        return rst;
    },
    readFile: function readFile(p) {
        var rst = '';
        p = (typeof p === 'undefined' ? 'undefined' : _typeof(p)) === 'object' ? _path2.default.join(p.dir, p.base) : p;
        try {
            rst = _fs2.default.readFileSync(p, 'utf-8');
        } catch (e) {
            rst = null;
        }
        return rst;
    },
    mkdir: function mkdir(name) {
        var rst = true;
        try {
            _fs2.default.mkdirSync(name);
        } catch (e) {
            rst = e;
        }
        return rst;
    },
    writeFile: function writeFile(p, data) {
        var opath = this.isString(p) ? _path2.default.parse(p) : p;
        if (!this.isDir(opath.dir)) {
            _mkdirp2.default.sync(opath.dir);
        }
        _fs2.default.writeFileSync(p, data);
    },
    copy: function copy(opath, ext, src, dist) {
        var target = this.getDistPath(opath, ext, src, dist);
        this.writeFile(target, this.readFile(_path2.default.join(opath.dir, opath.base)));
        var readable = _fs2.default.createReadStream(_path2.default.join(opath.dir, opath.base));
        var writable = _fs2.default.createWriteStream(target);
        readable.pipe(writable);
    },
    getNowDir: function getNowDir() {
        return _path2.default.resolve('./');
    },
    getRelative: function getRelative(opath) {
        return _path2.default.relative(this.currentDir, _path2.default.join(opath.dir, opath.base));
    },
    getDistPath: function getDistPath(opath, ext, src, dist) {
        var dir = '';
        src = src || cache.getSrc();
        dist = dist || cache.getDist();
        ext = ext ? '.' + ext : opath.ext;
        // 第三组件
        if (opath.dir.indexOf('' + _path2.default.sep + src + _path2.default.sep) === -1 && opath.dir.indexOf('node_modules') > 1) {
            dir = opath.dir.replace('node_modules', '' + dist + _path2.default.sep + 'npm') + _path2.default.sep;
        } else dir = (opath.dir + _path2.default.sep).replace(_path2.default.sep + src + _path2.default.sep, _path2.default.sep + dist + _path2.default.sep);
        return dir + opath.name + ext;
    },
    getModifiedTime: function getModifiedTime(p) {
        return this.isFile(p) ? +_fs2.default.statSync(p).mtime : false;
    },
    getConfig: function getConfig() {
        var config = cache.getConfig();
        if (config) return config;

        var configFile = _path2.default.join(this.currentDir, _path2.default.sep, 'h5.config.js');
        var configType = 'js';

        if (!this.isFile(configFile)) {
            configFile = _path2.default.join(this.currentDir, _path2.default.sep, '.wepyrc');

            config = this.readFile(configFile);
            try {
                config = JSON.parse(config);
            } catch (e) {
                config = null;
            }
        } else {
            config = require(configFile);
        }

        cache.setConfig(config);
        return config;
    },
    getIgnore: function getIgnore() {
        var ignoreFile = _path2.default.join(this.currentDir, _path2.default.sep, '.wepyignore');
        return this.isFile(ignoreFile) ? this.readFile(ignoreFile) : '';
    },
    getFiles: function getFiles() {
        var _this = this;

        var dir = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : process.cwd();
        var prefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';

        var cfiles = cache.getFileList(dir);
        if (cfiles) return cfiles;
        dir = _path2.default.normalize(dir);
        if (!_fs2.default.existsSync(dir)) {
            return [];
        }
        var files = _fs2.default.readdirSync(dir);
        var rst = [];
        files.forEach(function (item) {
            var filepath = dir + _path2.default.sep + item;
            var stat = _fs2.default.statSync(filepath);
            if (stat.isFile()) {
                rst.push(prefix + item);
            } else if (stat.isDirectory()) {
                rst = rst.concat(_this.getFiles(_path2.default.normalize(dir + _path2.default.sep + item), _path2.default.normalize(prefix + item + _path2.default.sep)));
            }
        });

        cache.setFileList(dir, rst);
        return rst;
    },
    getVersion: function getVersion() {
        var filepath = _path2.default.resolve(__dirname, '../package.json');
        var version = JSON.parse(this.readFile(filepath)).version;
        return version;
    },
    datetime: function datetime() {
        var date = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : new Date();
        var format = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'HH:mm:ss';

        var fn = function fn(d) {
            return ('0' + d).slice(-2);
        };
        if (date && this.isString(date)) {
            date = new Date(Date.parse(date));
        }
        var formats = {
            YYYY: date.getFullYear(),
            MM: fn(date.getMonth() + 1),
            DD: fn(date.getDate()),
            HH: fn(date.getHours()),
            mm: fn(date.getMinutes()),
            ss: fn(date.getSeconds())
        };
        return format.replace(/([a-z])\1+/ig, function (a) {
            return formats[a] || a;
        });
    },
    error: function error(msg) {
        this.log(msg, 'error', false);
    },
    warning: function warning(msg) {
        this.log(msg, 'warning', false);
    },
    success: function success(msg) {
        this.log(msg, 'succeess', false);
    },
    log: function log(msg, type) {
        var showTime = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

        var dateTime = showTime ? _safe2.default.gray('[' + this.datetime() + '] ') : '';
        if (this.isObject(msg) || this.isArray(msg)) {
            msg = JSON.stringify(msg);
        }
        if (type && this.isString(type)) {
            type = type.toUpperCase();
            if (type === 'ERROR') {
                console.error(_safe2.default.red('[Error] ' + msg));
                //console.log();
            } else if (type === 'WARNING') {
                console.error(_safe2.default.yellow('[WARNING] ' + msg));
                //console.log();
            } else if (type === 'SUCCEESS') {
                console.error(_safe2.default.blue('[SUCCEESS] ' + msg));
                //console.log();
            } else {
                var fn = _safe2.default[type] ? _safe2.default[type] : _safe2.default['info'];
                console.log(dateTime + fn('[' + type + ']') + ' ' + msg);
            }
        } else {
            console.log(dateTime + msg);
        }
    },
    output: function output(type, file, flag) {
        if (!flag) {
            flag = file.substr(file.lastIndexOf('.') + 1).toUpperCase();
            if (flag.length < 4) {
                var i = 4 - flag.length;
                while (i--) {
                    flag += ' ';
                }
            }
        }
        this.log(flag + ': ' + _path2.default.relative(this.currentDir, file), type);
    },
    downloadPic: function downloadPic(url) {

        // http.get(url, function (res) {
        //     var imgData = "";
        //     res.setEncoding("binary"); //一定要设置response的编码为binary否则会下载下来的图片打不开
        //     res.on("data", function (chunk) {
        //         imgData += chunk;
        //     });
        //     res.on("end", function () {
        //         fs.writeFile("./public/upload/downImg/logonew.png", imgData, "binary", function (err) {
        //             if (err) {
        //                 console.log("down fail");
        //             }
        //             console.log("down success");
        //         });
        //     });
        // });


    }
};
//# sourceMappingURL=util.js.map