'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _h = require('./h5');

var _h2 = _interopRequireDefault(_h);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
    /**
     * 创建测试版
     * 
     */
    init: function init() {
        // h5.getSence('http://b.eqxiu.com/s/2ocequKO');

        _h2.default.inputInfo().then(function (_ref) {
            var rl = _ref.rl,
                config = _ref.config;

            _h2.default.getSence(config.url);
            rl.close();
        });
    },

    /**
     * 建立发布版
     * 
     */
    build: function build() {},
    test: function test() {
        _h2.default.test();
    }
};
//# sourceMappingURL=compile.js.map