'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _readline = require('readline');

var _readline2 = _interopRequireDefault(_readline);

var _http = require('http');

var _http2 = _interopRequireDefault(_http);

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _cheerio = require('cheerio');

var _cheerio2 = _interopRequireDefault(_cheerio);

var _child_process = require('child_process');

var _child_process2 = _interopRequireDefault(_child_process);

var _util = require('./util');

var _util2 = _interopRequireDefault(_util);

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

var _download = require('download');

var _download2 = _interopRequireDefault(_download);

var _fsExtra = require('fs-extra');

var _fsExtra2 = _interopRequireDefault(_fsExtra);

var _copy = require('copy');

var _copy2 = _interopRequireDefault(_copy);

var _replacestream = require('replacestream');

var _replacestream2 = _interopRequireDefault(_replacestream);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _replaceInFile = require('replace-in-file');

var _replaceInFile2 = _interopRequireDefault(_replaceInFile);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * 临时存储文件
 */
var srcDir = 'src/';
var temDir = _util2.default.getNowDir() + '/dist/';
var cdnDir = _util2.default.getNowDir() + '/dist/cdn/';
var testDir = _util2.default.getNowDir() + '/test/';
var testCdnUrl = temDir + '/cdn/';
var downloadBaseUrl = 'http://res3.eqh5.com/';
var cdnUrl = 'https://s.bianlifeng.com/';
var mediaDownloadArr = [];

var allowMedias = ['.mp3', '.png', '.jpg', '.gif', '.svg'];
exports.default = {
    /**
     * 获取基础sence场景
     * 
     * @param {any} url 
     * @param {string} [file_name="tem"] 
     * @returns 
     */
    // 通过curl获取eh5的网页内容
    getEH5CodeByCurl: function getEH5CodeByCurl(url) {
        var file_name = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "tem";

        // util.log('正在获取内容' + url);
        // let curl = 'curl ' + url + ' > ' + temDir + file_name;
        // return util.exec(url)
        return new Promise(function (resolve, reject) {
            (0, _mkdirp2.default)(temDir, function (err) {
                if (err) _util2.default.error(err);else _util2.default.success('pow!');

                var curl = 'curl -o ' + temDir + file_name + ' ' + url;
                console.log('curl=>>>> ' + curl);
                var child = _child_process2.default.exec(curl, function (err, stdout, stderr) {

                    if (!err) {
                        _util2.default.success('获取 ' + url + ' 成功 ' + file_name);
                        resolve(file_name);
                    } else {
                        reject(err);
                    }
                });
            });
        });
    },

    // 获取sence id code
    getScenceObj: function getScenceObj(file_name) {

        _util2.default.log('解析sence场景内容......');
        return new Promise(function (resolve, reject) {
            _fs2.default.readFile(temDir + file_name, 'utf-8', function (err, data) {
                if (err) {
                    _util2.default.log(err);
                    reject(err);
                } else {
                    // 获取sence配置
                    var $ = _cheerio2.default.load(data);
                    var senceObjStr = $('script').eq(0).text();
                    senceObjStr = senceObjStr.replace("var __isServerRendered = true;", "");
                    senceObjStr = senceObjStr.replace("var scene =", "");
                    senceObjStr = senceObjStr.replace(";", "");
                    var sence = eval('(' + senceObjStr + ')');
                    // 处理配置信息
                    // 下载队列加入背景音乐


                    if (sence.bgAudio) {
                        var audioUrl = downloadBaseUrl + sence.bgAudio.url;
                        if (audioUrl) {
                            mediaDownloadArr.push(audioUrl);
                        }

                        // 替换
                        var temAudioUrl = cdnUrl + sence.bgAudio.url;
                        sence.bgAudio.url = temAudioUrl;
                    }

                    // property: { "slideNumber": false, "wxCount": 0, "eqAdType": 0, "ad": 0, "editorModel": 1, "wxClickFarmerCount": 0, "triggerLoop": true, "autoFlipTime": 3 },
                    sence.property = { "slideNumber": false, "wxCount": 0, "eqAdType": 0, "ad": 0, "editorModel": 1, "wxClickFarmerCount": 0, "triggerLoop": true, "autoFlipTime": 3 };
                    /**
                     * 替换index.html的内容
                     */
                    //  util.error(__dirname);
                    _fs2.default.readFile(__dirname + '/template/index.html', 'utf-8', function (err, data) {
                        if (err) {
                            _util2.default.log(err);
                            reject(err);
                        } else {
                            // 获取sence配置
                            var _$ = _cheerio2.default.load(data);
                            var innerHtml = "var scene =" + JSON.stringify(sence);
                            // util.log(innerHtml);
                            _$('#calling').html(innerHtml);
                            _util2.default.writeFile(temDir + 'index.html', _$.html());
                            resolve(sence);
                        }
                    });

                    // resolve(sence);
                }
            });
        });
    },
    getSenceJson: function getSenceJson(sence) {
        return new Promise(function (resolve, reject) {
            var jsonUrl = 'http://s1.eqxiu.com/eqs/page/' + sence.id + '?code=' + sence.code;
            _util2.default.log(jsonUrl);
            resolve(jsonUrl);
        });
    },


    /**
     * 获取相关多媒体 及 创建dist
     * 
     * @param {any} obj 
     * @returns 
     */

    // 复制相关文件到dist
    copyfiles: function copyfiles() {
        _fsExtra2.default.copy(__dirname + '/template/all.min.js', temDir + "all.min.js", function (err) {
            if (err) return _util2.default.error(err);
            _util2.default.log("success!");
        });
        _fsExtra2.default.copy(__dirname + '/template/all.min.css', temDir + "all.min.css", function (err) {
            if (err) return _util2.default.error(err);
            // util.log("success!")
        });
        _fsExtra2.default.copy(__dirname + '/template/iconfonts.min.css', temDir + "iconfonts.min.css", function (err) {
            if (err) return _util2.default.error(err);
            // util.log("success!")
        });
        _fsExtra2.default.copy(__dirname + '/template/flux.min.js', temDir + "flux.min.js", function (err) {
            if (err) return _util2.default.error(err);
            // util.log("success!")
        });
    },


    // 克隆与修改
    moreclone: function moreclone(obj) {
        var o;
        if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) == "object") {
            if (obj === null) {
                o = null;
            } else {

                if (obj instanceof Array) {
                    o = [];
                    for (var i = 0, len = obj.length; i < len; i++) {
                        o.push(this.moreclone(obj[i]));
                    }
                } else {
                    o = {};
                    for (var j in obj) {
                        o[j] = this.moreclone(obj[j]);
                    }
                }
            }
        } else {
            /**
             * 修改值
             * 保存到layout.json
             */
            o = obj;
            if (typeof o === 'string') {
                if (this.isAllowMedia(o)) {
                    this.buildDownLoadArr(o);
                    var mediaUrlObj = _url2.default.parse(o);
                    var seq = mediaUrlObj.pathname.indexOf('/') > -1 ? mediaUrlObj.pathname.split('/') : mediaUrlObj.pathname;
                    o = cdnUrl + (typeof seq === 'string' ? seq : seq[seq.length - 1]);
                }
            }
        }
        return o;
    },
    isAllowMedia: function isAllowMedia(str) {
        for (var i = 0; i < allowMedias.length; i++) {
            if (str.indexOf(allowMedias[i]) > -1) {
                return true;
            }
        }
        return false;
    },
    buildDownLoadArr: function buildDownLoadArr(o) {
        if (typeof o === 'string') {
            if (this.isAllowMedia(o)) {
                var mediaUrlObj = _url2.default.parse(o);
                mediaDownloadArr.push(downloadBaseUrl + mediaUrlObj.pathname);
            }
        }
    },
    getMedias: function getMedias() {
        // 获取用到的所有图片以及音乐文件链接
        var layObj = require(temDir + 'layout');
        var newObj = this.moreclone(layObj);

        if (newObj) {
            var objJson = JSON.stringify(newObj);
            _util2.default.writeFile(temDir + 'layout.json', objJson);
            this.downloadMedias();
        }
    },
    uniqueArr: function uniqueArr(array) {
        var r = [];
        for (var i = 0, l = array.length; i < l; i++) {
            for (var j = i + 1; j < l; j++) {
                if (array[i] === array[j]) j = ++i;
            }r.push(array[i]);
        }
        return r;
    },
    downloadMedias: function downloadMedias() {
        if (mediaDownloadArr && mediaDownloadArr.length > 0) {
            mediaDownloadArr = this.uniqueArr(mediaDownloadArr);
            console.log(this.uniqueArr(mediaDownloadArr));
            Promise.all(mediaDownloadArr.map(function (x) {
                return (0, _download2.default)(x, cdnDir);
            })).then(function () {
                _util2.default.log('多媒体下载完成,可以运行 test 进行测试页面查看');
            });
        }
    },


    /**
     * 获取场景
     * 
     * @param {any} url 
     */
    getSence: function getSence(url) {
        var _this = this;

        _util2.default.exec('rm -rf dist/* && rm -rf test/*').then(function () {
            return _this.getEH5CodeByCurl(url, 'index.html');
        }).then(function (file_name) {
            return _this.getScenceObj(file_name);
        }).then(function (senceObj) {
            return _this.getSenceJson(senceObj);
        }).then(function (jsonUrl) {
            return _this.getEH5CodeByCurl(jsonUrl, 'layout.json');
        }).then(function () {
            _this.copyfiles();
            _this.getMedias();
        }).catch(function (err) {
            _util2.default.error('获取sence出错' + err);
        });
    },

    /**
     * 获取配置信息
     * 
     * @returns 
     */
    inputInfo: function inputInfo() {
        // process.chdir(name);
        return new Promise(function (resolve, reject) {
            var rl = _readline2.default.createInterface({
                input: process.stdin,
                output: process.stdout,
                terminal: true
            });
            var config = {};
            rl.question('请输入eH5的预览网址: ', function (url) {
                _util2.default.log('eH5\u7684\u9884\u89C8\u7F51\u5740\u4E3A: ' + url);
                config.url = url;
                rl.question('请配置cdn地址: ', function (scdnUrl) {
                    _util2.default.log('cdn\u5730\u5740\u4E3A: ' + scdnUrl);
                    config.cdnUrl = scdnUrl;
                    cdnUrl = scdnUrl;
                    _util2.default.writeFile('config.json', JSON.stringify(config));
                    resolve({ rl: rl, config: config });
                });
            });
        });
    },


    /**
     * 测试启动 查看效果
     */
    testClone: function testClone(obj) {
        var o;
        if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) == "object") {
            if (obj === null) {
                o = null;
            } else {

                if (obj instanceof Array) {
                    o = [];
                    for (var i = 0, len = obj.length; i < len; i++) {
                        o.push(this.testClone(obj[i]));
                    }
                } else {
                    o = {};
                    for (var j in obj) {
                        o[j] = this.testClone(obj[j]);
                    }
                }
            }
        } else {
            /**
             * 修改值
             * 保存到layout.json
             */
            o = obj;
            if (typeof o === 'string') {
                if (this.isAllowMedia(o)) {
                    var mediaUrlObj = _url2.default.parse(o);
                    var seq = mediaUrlObj.pathname.indexOf('/') > -1 ? mediaUrlObj.pathname.split('/') : mediaUrlObj.pathname;
                    o = typeof seq === 'string' ? seq : seq[seq.length - 1];
                }
            }
        }
        return o;
    },
    copyOne: function copyOne(o, n) {
        return new Promise(function (resolve, reject) {
            (0, _copy2.default)(o, n, function (err, file) {
                resolve();
            });
        });
    },
    testIndex: function testIndex(file_name) {
        return new Promise(function (resolve, reject) {
            _fs2.default.readFile(testDir + file_name, 'utf-8', function (err, data) {
                if (err) {
                    _util2.default.log(err);
                    reject(err);
                } else {
                    // 获取sence配置
                    var $ = _cheerio2.default.load(data);
                    var senceObjStr = $('script').eq(0).text();
                    senceObjStr = senceObjStr.replace("var __isServerRendered = true;", "");
                    senceObjStr = senceObjStr.replace("var scene =", "");
                    senceObjStr = senceObjStr.replace(";", "");
                    var sence = eval('(' + senceObjStr + ')');

                    var config = require(_util2.default.getNowDir() + '/config');
                    // 替换
                    if (sence.bgAudio && sence.bgAudio.url) {
                        var temAudioUrl = sence.bgAudio.url.replace(config.cdnUrl, '');
                        sence.bgAudio.url = temAudioUrl;
                    }

                    // property: { "slideNumber": false, "wxCount": 0, "eqAdType": 0, "ad": 0, "editorModel": 1, "wxClickFarmerCount": 0, "triggerLoop": true, "autoFlipTime": 3 },
                    sence.property = { "slideNumber": false, "wxCount": 1000, "eqAdType": 0, "ad": 0, "editorModel": 1, "wxClickFarmerCount": 0, "triggerLoop": true, "autoFlipTime": 3 };
                    /**
                     * 替换index.html的内容
                     */
                    // 获取sence配置
                    var innerHtml = "var scene =" + JSON.stringify(sence);
                    // util.log(innerHtml);
                    $('#calling').html(innerHtml);
                    _util2.default.writeFile(testDir + 'index.html', $.html());
                    resolve();
                }
            });
        });
    },
    test: function test() {
        var _this2 = this;

        var copyPromises = [this.copyOne('dist/*', 'test/'), this.copyOne('dist/cdn/*', 'test/')];
        Promise.all(copyPromises).then(function () {
            return _this2.testIndex('index.html');
        }).then(function () {
            var layObj = require(temDir + 'layout');
            var newObj = _this2.testClone(layObj);

            if (newObj) {
                var objJson = JSON.stringify(newObj);
                _util2.default.writeFile(testDir + 'layout.json', objJson);
                _util2.default.success('等待启动服务器....');
            }
        }).then(function () {
            _util2.default.success('启动服务器....');
            var serverstart = 'cd test/ && anywhere';
            var child = _child_process2.default.exec(serverstart, function (err, stdout, stderr) {
                if (!err) {
                    _util2.default.success('启动成功');
                } else {
                    _util2.default.error(err);
                }
            });
        }).catch(function (err) {
            _util2.default.error(err);
        });
    }
};
//# sourceMappingURL=h5.js.map