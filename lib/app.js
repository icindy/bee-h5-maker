'use strict';

var _commander = require('commander');

var _commander2 = _interopRequireDefault(_commander);

var _compile = require('./compile');

var _compile2 = _interopRequireDefault(_compile);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * 接收参数
 */
_commander2.default.version(require('../package.json').version).usage('[option] [project name]');

_commander2.default.command('init').action(function () {
    _compile2.default.init(_commander2.default);
}).description('初始化项目');

_commander2.default.command('test').action(function () {
    _compile2.default.test(_commander2.default);
}).description('测试使用');

_commander2.default.parse(process.argv);
// 空配置
var pname = _commander2.default.args[0];
if (!pname) _commander2.default.help();
//# sourceMappingURL=app.js.map