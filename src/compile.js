import h5 from './h5'

export default {
    /**
     * 创建测试版
     * 
     */
    init (){
        // h5.getSence('http://b.eqxiu.com/s/2ocequKO');
                
        h5.inputInfo().then(function({rl, config}){
            h5.getSence(config.url);
            rl.close();
        });
    },
    /**
     * 建立发布版
     * 
     */
    build(){

    },

    test (){
        h5.test();
    }
}