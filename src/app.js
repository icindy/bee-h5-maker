import commander from 'commander';
import compile from './compile';
/**
 * 接收参数
 */
commander
    .version(require('../package.json').version)
    .usage('[option] [project name]');

commander.command('init').action(() => {
    compile.init(commander);
}).description('初始化项目');

commander.command('test').action(() => {
    compile.test(commander);
}).description('测试使用');

commander.parse(process.argv);
// 空配置
var pname = commander.args[0];
if(!pname) commander.help();