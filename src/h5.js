import readline from 'readline'
import http from 'http'
import fs from 'fs'
import cheerio from 'cheerio'
import child_process from 'child_process'
import util from './util'
import mkdirp from 'mkdirp'
import url from 'url'
import download from 'download'
import fsExtra from 'fs-extra'
import copy from 'copy'
import replaceStream from 'replacestream'
import path from 'path'
import replace from 'replace-in-file'
/**
 * 临时存储文件
 */
let srcDir = 'src/';
let temDir = util.getNowDir() + '/dist/';
let cdnDir = util.getNowDir() + '/dist/cdn/'
let testDir = util.getNowDir() + '/test/';
let testCdnUrl = temDir + '/cdn/';
let downloadBaseUrl = 'http://res3.eqh5.com/';
let cdnUrl = 'https://s.bianlifeng.com/';
let mediaDownloadArr = [];

let allowMedias = ['.mp3', '.png', '.jpg', '.gif', '.svg'];
export default {
    /**
     * 获取基础sence场景
     * 
     * @param {any} url 
     * @param {string} [file_name="tem"] 
     * @returns 
     */
    // 通过curl获取eh5的网页内容
    getEH5CodeByCurl(url, file_name = "tem") {
        // util.log('正在获取内容' + url);
        // let curl = 'curl ' + url + ' > ' + temDir + file_name;
        // return util.exec(url)
        return new Promise(function (resolve, reject) {
            mkdirp(temDir, function (err) {
                if (err) util.error(err)
                else util.success('pow!')

                let curl = 'curl -o ' + temDir + file_name + ' ' + url;
                console.log('curl=>>>> ' + curl);
                let child = child_process.exec(curl, function (err, stdout, stderr) {

                    if (!err) {
                        util.success('获取 ' + url + ' 成功 ' + file_name);
                        resolve(file_name);
                    } else {
                        reject(err);
                    }
                });
            });
        });

    },
    // 获取sence id code
    getScenceObj(file_name) {

        util.log('解析sence场景内容......');
        return new Promise((resolve, reject) => {
            fs.readFile(temDir + file_name, 'utf-8', function (err, data) {
                if (err) {
                    util.log(err);
                    reject(err);
                } else {
                    // 获取sence配置
                    let $ = cheerio.load(data);
                    let senceObjStr = $('script').eq(0).text();
                    senceObjStr = senceObjStr.replace("var __isServerRendered = true;", "");
                    senceObjStr = senceObjStr.replace("var scene =", "");
                    senceObjStr = senceObjStr.replace(";", "");
                    let sence = eval('(' + senceObjStr + ')');
                    // 处理配置信息
                    // 下载队列加入背景音乐


                    if (sence.bgAudio) {
                        let audioUrl = downloadBaseUrl + sence.bgAudio.url;
                        if (audioUrl) {
                            mediaDownloadArr.push(audioUrl);
                        }

                        // 替换
                        let temAudioUrl = cdnUrl + sence.bgAudio.url;
                        sence.bgAudio.url = temAudioUrl;
                    }

                    // property: { "slideNumber": false, "wxCount": 0, "eqAdType": 0, "ad": 0, "editorModel": 1, "wxClickFarmerCount": 0, "triggerLoop": true, "autoFlipTime": 3 },
                    sence.property = { "slideNumber": false, "wxCount": 0, "eqAdType": 0, "ad": 0, "editorModel": 1, "wxClickFarmerCount": 0, "triggerLoop": true, "autoFlipTime": 3 };
                    /**
                     * 替换index.html的内容
                     */
                    //  util.error(__dirname);
                    fs.readFile(__dirname + '/template/index.html', 'utf-8', function (err, data) {
                        if (err) {
                            util.log(err);
                            reject(err);
                        } else {
                            // 获取sence配置
                            let $ = cheerio.load(data);
                            var innerHtml = "var scene =" + JSON.stringify(sence);
                            // util.log(innerHtml);
                            $('#calling').html(innerHtml);
                            util.writeFile(temDir + 'index.html', $.html());
                            resolve(sence);
                        }
                    })

                    // resolve(sence);
                }
            })
        });

    },

    getSenceJson(sence) {
        return new Promise((resolve, reject) => {
            let jsonUrl = 'http://s1.eqxiu.com/eqs/page/' + sence.id + '?code=' + sence.code;
            util.log(jsonUrl);
            resolve(jsonUrl);
        });
    },

    /**
     * 获取相关多媒体 及 创建dist
     * 
     * @param {any} obj 
     * @returns 
     */

    // 复制相关文件到dist
    copyfiles() {
        fsExtra.copy(__dirname + '/template/all.min.js', temDir + "all.min.js", err => {
            if (err) return util.error(err)
            util.log("success!")
        });
        fsExtra.copy(__dirname + '/template/all.min.css', temDir + "all.min.css", err => {
            if (err) return util.error(err)
            // util.log("success!")
        });
        fsExtra.copy(__dirname + '/template/iconfonts.min.css', temDir + "iconfonts.min.css", err => {
            if (err) return util.error(err)
            // util.log("success!")
        });
        fsExtra.copy(__dirname + '/template/flux.min.js', temDir + "flux.min.js", err => {
            if (err) return util.error(err)
            // util.log("success!")
        });
    },

    // 克隆与修改
    moreclone(obj) {
        var o;
        if (typeof obj == "object") {
            if (obj === null) {
                o = null;
            } else {

                if (obj instanceof Array) {
                    o = [];
                    for (let i = 0, len = obj.length; i < len; i++) {
                        o.push(this.moreclone(obj[i]));
                    }
                } else {
                    o = {};
                    for (let j in obj) {
                        o[j] = this.moreclone(obj[j]);
                    }
                }
            }
        } else {
            /**
             * 修改值
             * 保存到layout.json
             */
            o = obj;
            if (typeof o === 'string') {
                if (this.isAllowMedia(o)) {
                    this.buildDownLoadArr(o);
                    var mediaUrlObj = url.parse(o);
                    var seq = mediaUrlObj.pathname.indexOf('/') > -1 ? mediaUrlObj.pathname.split('/') : mediaUrlObj.pathname;
                    o = cdnUrl + (typeof seq === 'string' ? seq : seq[seq.length - 1]);
                }
            }
        }
        return o;
    },
    isAllowMedia(str) {
        for (let i = 0; i < allowMedias.length; i++) {
            if (str.indexOf(allowMedias[i]) > -1) {
                return true;
            }
        }
        return false;
    },
    buildDownLoadArr(o) {
        if (typeof o === 'string') {
            if (this.isAllowMedia(o)) {
                let mediaUrlObj = url.parse(o);
                mediaDownloadArr.push(downloadBaseUrl + mediaUrlObj.pathname);
            }
        }
    },
    getMedias() {
        // 获取用到的所有图片以及音乐文件链接
        let layObj = require(temDir + 'layout');
        let newObj = this.moreclone(layObj);

        if (newObj) {
            let objJson = JSON.stringify(newObj);
            util.writeFile(temDir + 'layout.json', objJson);
            this.downloadMedias();
        }
    },
    uniqueArr(array) {
        var r = [];
        for (var i = 0, l = array.length; i < l; i++) {
            for (var j = i + 1; j < l; j++)
                if (array[i] === array[j]) j = ++i;
            r.push(array[i]);
        }
        return r;
    },
    downloadMedias() {
        if (mediaDownloadArr && mediaDownloadArr.length > 0) {
            mediaDownloadArr = this.uniqueArr(mediaDownloadArr);
            console.log(this.uniqueArr(mediaDownloadArr));
            Promise.all(mediaDownloadArr.map(x => download(x, cdnDir))).then(() => {
                util.log('多媒体下载完成,可以运行 test 进行测试页面查看');
            });
        }
    },

    /**
     * 获取场景
     * 
     * @param {any} url 
     */
    getSence(url) {
        util.exec('rm -rf dist/* && rm -rf test/*')
            .then(() => {
                return this.getEH5CodeByCurl(url, 'index.html')
            })
            .then((file_name) => {
                return this.getScenceObj(file_name);
            }).then((senceObj) => {
                return this.getSenceJson(senceObj);
            }).then((jsonUrl) => {
                return this.getEH5CodeByCurl(jsonUrl, 'layout.json');
            }).then(() => {
                this.copyfiles();
                this.getMedias();
            }).catch(function (err) {
                util.error('获取sence出错' + err);
            });
    },
    /**
     * 获取配置信息
     * 
     * @returns 
     */
    inputInfo() {
        // process.chdir(name);
        return new Promise(function (resolve, reject) {
            const rl = readline.createInterface({
                input: process.stdin,
                output: process.stdout,
                terminal: true
            });
            let config = {};
            rl.question('请输入eH5的预览网址: ', function (url) {
                util.log(`eH5的预览网址为: ${url}`);
                config.url = url;
                rl.question('请配置cdn地址: ', function (scdnUrl) {
                    util.log(`cdn地址为: ${scdnUrl}`);
                    config.cdnUrl = scdnUrl;
                    cdnUrl = scdnUrl;
                    util.writeFile('config.json', JSON.stringify(config));
                    resolve({ rl, config });
                });
            });
        })
    },

    /**
     * 测试启动 查看效果
     */
    testClone(obj) {
        var o;
        if (typeof obj == "object") {
            if (obj === null) {
                o = null;
            } else {

                if (obj instanceof Array) {
                    o = [];
                    for (let i = 0, len = obj.length; i < len; i++) {
                        o.push(this.testClone(obj[i]));
                    }
                } else {
                    o = {};
                    for (let j in obj) {
                        o[j] = this.testClone(obj[j]);
                    }
                }
            }
        } else {
            /**
             * 修改值
             * 保存到layout.json
             */
            o = obj;
            if (typeof o === 'string') {
                if (this.isAllowMedia(o)) {
                    var mediaUrlObj = url.parse(o);
                    var seq = mediaUrlObj.pathname.indexOf('/') > -1 ? mediaUrlObj.pathname.split('/') : mediaUrlObj.pathname;
                    o = (typeof seq === 'string' ? seq : seq[seq.length - 1]);
                }
            }
        }
        return o;
    },
    copyOne(o, n) {
        return new Promise((resolve, reject) => {
            copy(o, n, (err, file) => {
                resolve();
            });
        });
    },
    testIndex(file_name) {
        return new Promise((resolve, reject) => {
            fs.readFile(testDir + file_name, 'utf-8', function (err, data) {
                if (err) {
                    util.log(err);
                    reject(err);
                } else {
                    // 获取sence配置
                    let $ = cheerio.load(data);
                    let senceObjStr = $('script').eq(0).text();
                    senceObjStr = senceObjStr.replace("var __isServerRendered = true;", "");
                    senceObjStr = senceObjStr.replace("var scene =", "");
                    senceObjStr = senceObjStr.replace(";", "");
                    let sence = eval('(' + senceObjStr + ')');

                    let config = require(util.getNowDir() + '/config');
                    // 替换
                    if(sence.bgAudio && sence.bgAudio.url){
                      let temAudioUrl = sence.bgAudio.url.replace(config.cdnUrl, '');
                        sence.bgAudio.url = temAudioUrl;
                    }
                    
                    // property: { "slideNumber": false, "wxCount": 0, "eqAdType": 0, "ad": 0, "editorModel": 1, "wxClickFarmerCount": 0, "triggerLoop": true, "autoFlipTime": 3 },
                    sence.property = { "slideNumber": false, "wxCount": 1000, "eqAdType": 0, "ad": 0, "editorModel": 1, "wxClickFarmerCount": 0, "triggerLoop": true, "autoFlipTime": 3 };
                    /**
                     * 替换index.html的内容
                     */
                    // 获取sence配置
                    var innerHtml = "var scene =" + JSON.stringify(sence);
                    // util.log(innerHtml);
                    $('#calling').html(innerHtml);
                    util.writeFile(testDir + 'index.html', $.html());
                    resolve();
                }
            })
        });
    },
    test() {
        let copyPromises = [this.copyOne('dist/*', 'test/'), this.copyOne('dist/cdn/*', 'test/')]
        Promise.all(copyPromises).then(() => {
            return this.testIndex('index.html');
        }).then(() => {
            let layObj = require(temDir + 'layout');
            let newObj = this.testClone(layObj);

            if (newObj) {
                let objJson = JSON.stringify(newObj);
                util.writeFile(testDir + 'layout.json', objJson);
                util.success('等待启动服务器....');
            }
        }).then(() => {
            util.success('启动服务器....');
            let serverstart = 'cd test/ && anywhere';
            let child = child_process.exec(serverstart, function (err, stdout, stderr) {
                if (!err) {
                    util.success('启动成功');
                } else {
                    util.error(err);
                }
            });
        }).catch((err) => {
            util.error(err);
        });
    }

}