## beeh5maker

eh5 转换工具

## 安装步骤

```shell
npm install beeh5maker -g
```

## 使用

```shell
// 1.创建文件夹
mkdir test
// 2.执行
beeh5maker init
// 3.按照指示输入eh5的预览网址
// 4.事先建立cdn地址,并填写

```
## 测试

```shell
// test文件夹下执行
beeh5maker test
```

## 部署

test文件夹下`dist/cdn/*`多媒体资源复制拷贝到你的cdn服务
test下的`dist/文件(不包含cdn)`拷贝到服务器上
完成

